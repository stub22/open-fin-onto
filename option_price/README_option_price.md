# Option Pricing using Functions (FP) and Ontology Grounding

## Background 
* [Intro to option pricing models](https://corporatefinanceinstitute.com/resources/knowledge/valuation/option-pricing-models/) at Corporate Finance Institute
* [Option Valuation](https://www.investopedia.com/walkthrough/corporate-finance/5/risk-management/option-valuation.aspx) at Investopedia
* [Functional option pricing experiment](https://medium.com/@calesennett/pricing-options-with-haskell-c3f68e13939a) (uses Haskell), by Cale Sennett
* [Backround on this O-F-O project and FIBO](../README.md) - our top README page.
### Our Approach

* Primary API feature is genOptPricer(optScenario), which generate a standalone price function ("optPricer") for each scenario.
  * A genOptPricer impl may use any programming language.  We use Scala, Rust, and Idris (derived from Haskell). 
* Test client runs genOptPricer (and resulting optPricers) over many price scenarios, collects results, characterizes them.
* For general introduction, skip to next section.  For the salty, here are some highlight links to fun and interesting specific pieces in our work:
  * [FIBO grounding for genOptPricer()](../fibo_study/opt_pricer_grnd_fibo.md) - initial analysis
  * Scala toy project for FIBO onto query:  https://github.com/stub22/axiomagic/tree/master/adaxmvn/axmgc_dmo_fibo
    * [OntDatMdl.scala](https://github.com/stub22/axiomagic/blob/master/adaxmvn/axmgc_dmo_fibo/src/main/scala/axmgc/dmo/fin/ontdmp/OntDatMdl.scala) accesses in-memory RDF graphs directly, without SPARQL.

#### Generate compiled Pricer for given fixed inputs: (Underlying,  OptCube, EvalPrefs)

All inputs are explicit - there is no hidden "database" of state behind the 
scenes, other than what is hardcoded into the software.

Changing any input data requires a complete regeneration of the Pricer.

* Input: <b>Price Scenario</b> is composed of three pieces:  Underlying, OptCube, EvalPrefs.
  * <b>Underlying</b> = Graph data set relevant to pricing a single underlying publicly traded security (equity, ETF, ETN).
    * Set of graphs providing asserted information about this security, and relevant info from the rest of the economic world.
      * Mixture of reference data, naive deterministic assertions, and modal + stochastic models.
      * Any a priori behavioral assumptions about the security (e.g. its volatility) should be explicitly stated here.
      * Information is presumed "current" at the evaluation time of the Strip, except as explicitly noted otherwise.  Any desired uncertainty on this point must be modeled into the underlying.
      * Ordinarily the current price of the underlying is included, hence the current <i>intrinsic</i> value of the options are easy to calculate.
    * Mix of open + closed world semantics as interpreted by genPricer.
    * Curation subject to scrutiny and debate by people using various tools.
  * <b>OptCube</b> = A finite set of option contracts (on the Underlying) to be priced, at a particular time, on a particular exchange
    * Described as a parameter space of strikes, expirations, and rights (e.g. euro-call, amero-put)
    * Defines single trading as-of time for price : optPriceTime
      * Arbitrary limitation intended to make computing and analysis more tractable. 
    * Incorporates by reference all rules of the relevant exchange (ideally).
    * Closed world semantics, encodable in any base format.
  * <b>EvalPrefs</b> = A set of calculation preferences, e.g. "Use Black-Scholes method".
    * Generally evalPrefs are simple RDF graphs, interpreted internally as having closed-world semantics.
      * The values in EvalPrefs may have dramatic impact on interpretation of Underlying graphs, including open-world semantics.
    * Broadly, adding preference data is conceptually similar to adding code.
    * Caution:  Depending on impl, security and reliability holes can exist in preference mechanisms.
* Output: <b>Opt Pricer</b> <i>function</i> 
  * <b>OptPricer</b> = A single function able to quickly compute "our price" for any option in the scenario OptCube.
    * Pricer should be a [pure function](https://en.wikipedia.org/wiki/Pure_function) which always yields the same output price for the same element of the OptCube.
    * The time of price evaluation is fixed by the OptCube.optPriceTime input, at <i>generation</i> time.
      * There is no way to "update" a Pricer without regenerating it from scratch.
    * Generating a Pricer function may take a long time, but once a Pricer function exists, it must execute quickly.
    * Streamed pricer function closures are suitable for use in some High Performance Computing environments.

Contextual economic information (e.g. risk-free interest model, various forecasts)
is usually supplied as part of Underlying.  Small pieces may also find their way
into EvalPrefs, but not into OptCube, which is narrowly defined as an indexing
parameter space.

#### Differentiating OptPricer w.r.t. EvalPrefs
Our process of calculating an output Pricer may be thought of as a process of 
appending derived information to EvalPrefs until such time as Pricer is fully 
specified, and hence executable.  Thus we may generally make the calculation
easier by supplying more complete Prefs.  These Prefs narrow the context the
Pricer-Gen needs to consider.   

Inversely, an executable Pricer may be seen as a complete (but perhaps 
implicit) specifcation of all relevant Prefs (as applied to the given 
Underlying and OptCube)

## FIBO ontology grounding of genPricer inputs
 * [FIBO grounding for genPricer() - initial analysis](../fibo_study/opt_pricer_grnd_fibo.md)