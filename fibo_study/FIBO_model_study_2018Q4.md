Preliminary study notes - DRAFT - not for publication 

## Our motivation
 * Understand the FIBO ontology model structure from a bottom-up, zoological perspective.
 
## Our procedure 
 * Load fibo ontology model into a Scala (/java) program equipped with Jena RDF library stack.
 * Query ontology to determine gross characteristics, using Jena API and avoiding SPARQL.
 * First pass:  Using FIBO production 2018Q4 via single consolidated ontology model file:  **all.ttl**
   * all.ttl is found inside prod.ttl.zip. which may be downloaded from:
     * https://spec.edmcouncil.org/fibo/ontology/master/2018Q4/prod.ttl.zip
   * We copied all.ttl into an experimental open source subproject inside Axiomagic, called "Axiomagic Demo Fibo". 
     * Project folder axmgc_dmo_fibo is here:  https://github.com/stub22/axiomagic/tree/master/adaxmvn/axmgc_dmo_fibo 
     * The FIBO ontology copy is here: https://github.com/stub22/axiomagic/blob/master/adaxmvn/axmgc_dmo_fibo/src/main/resources/gdat/fibo_ont/fibo_2018Q4_all_4MB.ttl
     * This location allows the file to be easily loaded as a java classpath resource.
## Our results - some initial gross statistics
 * All results derived from Jena v3.8.0 interaction with contents of fibo_2018Q4_all_4MB.ttl.
 * model.size() = 65053 RDF statements (= 65053 graph triples of the form {s, p, o} )
 * owl:import statements:  1519
 * owl:import unique URI targets:  189
 * Unique subject resource URIs: 9933 
 * Typed subject resources:  9575 = about 95% of the total number of subjects
 * rdf:type statements:  16009 = nearly a quarter of the total statements in the model
 * Domain-specific datatypes *used* in rdf:type statements: Approx 122

 * Unique properties *used* (from all namespaces): 160
 
Top 25 Properties in usage frequency, highest at bottom.  
 * Second column is statement count.  
 * Third column is pct of total statements, which is 65053 as shown at bottom and above.
 * These 25 props (out of 160) are used in 85.4% of the total statements.

<img src="_img_snp/fibo_props_freq_top25.png"/>
 
 * Below is an ugly summary of inhabitants of each kind of general datatype, pasted straight from our log! 
 * key 1 [http://www.omg.org/techprocess/ab/SpecificationMetadata/Module] bound to 66 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FND/TransactionsExt/MetadataFNDTransactionsExt/TransactionsExtModule, https://spec.edmcouncil.org/fibo/ontology/IND/EconomicIndicators/MetadataINDEconomicIndicators/EconomicIndicatorsModule, https://spec.edmcouncil.org/fibo/ontology/FND/PartiesExt/MetadataFNDPartiesExt/PartiesExtModule)
 * key 2 [http://www.w3.org/2000/01/rdf-schema#Datatype] bound to 28 values.  First 3 are: List(95e43016b0ffe4a3cc8e59181c8caf7c, 86c8fe7f2540ef47bf78777f934b6fd6, 34f056339c65b9aee1d71a407fa00e73)
 * key 3 [http://www.w3.org/2002/07/owl#AllDifferent] bound to 1 values.  First 3 are: List(0811b1e156e1b4b1a85fbd6c8a9bf5aa)
 * key 4 [http://www.w3.org/2002/07/owl#AnnotationProperty] bound to 11 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FND/Utilities/AnnotationVocabulary/synonym, https://spec.edmcouncil.org/fibo/ontology/FND/Utilities/AnnotationVocabulary/adaptedFrom, https://spec.edmcouncil.org/fibo/ontology/FND/Utilities/AnnotationVocabulary/modifiedBy)
 * key 5 [http://www.w3.org/2002/07/owl#Class] bound to 1670 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/IND/InterestRates/CommonInterestRates/CHF-LIBOR-BBA-Bloomberg, https://spec.edmcouncil.org/fibo/ontology/FND/Law/LegalCapacity/ContractualRight, https://spec.edmcouncil.org/fibo/ontology/FND/Law/LegalCapacity/ContingentRight)
 * key 6 [http://www.w3.org/2002/07/owl#DatatypeProperty] bound to 131 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/SEC/Securities/SecuritiesIssuance/hasSeries, https://spec.edmcouncil.org/fibo/ontology/FND/DatesAndTimes/FinancialDates/hasCount, https://spec.edmcouncil.org/fibo/ontology/FND/Arrangements/Arrangements/hasObservedDateTime)
 * key 7 [http://www.w3.org/2002/07/owl#FunctionalProperty] bound to 15 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FND/DatesAndTimes/FinancialDates/hasStartDate, https://spec.edmcouncil.org/fibo/ontology/FND/AgentsAndPeople/People/hasDateOfBirth, https://spec.edmcouncil.org/fibo/ontology/FND/DatesAndTimes/FinancialDates/hasEndDate)
 * key 8 [http://www.w3.org/2002/07/owl#InverseFunctionalProperty] bound to 1 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FND/Accounting/CurrencyAmount/hasBaseCurrency)
 * key 9 [http://www.w3.org/2002/07/owl#NamedIndividual] bound to 5299 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FBC/FunctionalEntities/MarketsIndividuals/XMAU, https://spec.edmcouncil.org/fibo/ontology/FND/Accounting/ISO4217-CurrencyCodes/SOS, https://spec.edmcouncil.org/fibo/ontology/FND/Accounting/ISO4217-CurrencyCodes/KPW)
 * key 10 [http://www.w3.org/2002/07/owl#ObjectProperty] bound to 448 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FND/OwnershipAndControl/Ownership/owns, https://spec.edmcouncil.org/fibo/ontology/FBC/DebtAndEquities/Debt/hasBorrower, https://spec.edmcouncil.org/fibo/ontology/FBC/DebtAndEquities/Debt/hasExtensionProvision)
 * key 11 [http://www.w3.org/2002/07/owl#Ontology] bound to 179 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FND/Relations/Relations/, https://spec.edmcouncil.org/fibo/ontology/FBC/FunctionalEntities/Markets/, https://spec.edmcouncil.org/fibo/ontology/FND/Places/Locations/)
 * key 12 [http://www.w3.org/2002/07/owl#Restriction] bound to 1805 values.  First 3 are: List(19e5b6bd551dc331b0b86b980229a4d6, 5b023f457254df094800d3f7d911dd55, 8679a8bef1c0ea984231a03f0a49e7cb)
 * key 13 [http://www.w3.org/2002/07/owl#SymmetricProperty] bound to 1 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/DER/DerivativesContracts/Swaps/exchanges)
 * key 14 [http://www.w3.org/2002/07/owl#TransitiveProperty] bound to 1 values.  First 3 are: List(https://spec.edmcouncil.org/fibo/ontology/FND/Relations/Relations/isPartOf)
