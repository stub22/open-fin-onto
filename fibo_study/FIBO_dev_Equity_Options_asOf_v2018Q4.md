# EquityOption Pricing Example - Collecting Relevant FIBO Vocab

#### Context of Inquiry
* [Coarse analysis of FIBO grounding for our OptPricer](https://gitlab.com/stub22/open-fin-onto/blob/master/fibo_study/opt_pricer_grnd_fibo.md)

#### Specifically relevant FIBO artifacts - starting points
* The FIBO ontology for <b>EquityOptions</b> is present only in the "dev" stream, as of 2018Q4.
* Ontology URI: https://spec.edmcouncil.org/fibo/ontology/DER/AssetDerivatives/EquityOptions/
* Asset Derivatives folder in "latest" file tree:  https://spec.edmcouncil.org/fibo/ontology/master/latest//DER/AssetDerivatives/
* HTML Doc (can be slow to load, due to VOWL plugin):  https://spec.edmcouncil.org/fibo/widoco/master/latest/DER/AssetDerivatives/EquityOptions/index-en.html

#### Collecting relevant vocabulary terms - ongoing
* Relevant vocab is spread over numerous FIBO ontology subgraphs.  
* One current idea (2019-02-28) is to collect all the useful (to us) vocabulary into a physical subset of the FIBO triple set.
* Note that the dev-2018Q4 version of "all.ttl" does not contain the contents of the EquityOptions graph, so it is not a viable starting point.

