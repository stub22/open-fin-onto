# FIBO Grounding of Option Pricer input scenario data
 * <i>Background</i>
   * This page discusses grounding of our <code>Option Pricer</code> example:  [README_option_price.md](../option_price/README_option_price.md)
   * For introduction to FIBO:  See links in our top [README](../README.md) page.
   * For software implementors:  Our experiment in querying FIBO ontology directly from Scala code (not using SPARQL) is [discussed here](../fibo_study/FIBO_model_study_2018Q4.md).
 * <b>Topic Question</b>:  How much descriptive power does FIBO vocab give us in specifying our three inputs to genPricer?
   * Underlying - (open+closed world) all fundamentals and history of a single equity security, plus all relevant economic context (up to any scale desired).
   * OptCube - (closed world) parameter space of option strikes, expirys, rights (call/put + contract rules).
   * EvalPrefs - whatever else we want or need to tell the genPricer code - only tangentially relevant here.

Preliminary decomposition of the three components are now discussed in separate sections.

### <code>Underlying</code> grounded in FIBO 

Models all relevant objective(-ish?) financial information available, within our capacity.

 * Underlying = (UE, UG) = a consistent pair of semantic graphs.
   * UE: underlying equity = all fundamentals and history of a single equity security, one of:
     * UECS = underlying exchange traded common stock of a public company.
     * UETF = underlying exchange traded fund = ETF = pledged basket of shares in other equities.
     * UETN = underlying exchange traded note = ETN = contractual security issued by investment bank.
     * UIDX = underlying exchange-published index for which tradable options exist, e.g. SPX, VIX.
   * UG: underlying global = all relevant economic context and news, e.g. for industry, region, central bank.
 * The UE portion may include any amount and granularity of "current" and historical 
information about the security, the exchange, the company and its markets, suppliers, competitors.  
  * UE and UG graphs should either cover disjoint topics, or attempt to be consistent.  
    * genPricer() does not look for inconsistency in UE + UG.  That could be done by a separate tool.
 * Each of UE and UG graphs may be arbitrarily large or complex, so in practice they 
are chosen to be of a <i>sensible</i> scope, given data we have and what our genPricer() knows how to use.
   * We start with the pieces of UE and UG that are easiest for us to say directly using the FIBO vocab.
     * CURRENT TOPIC Feb 26:  What pieces are those, exactly? 
 * Note that exactly one "now" timepoint  is encoded in the optPriceTime of the OptCube.
   * The priceHistory encoded in UE may include any prices up to the moment of priceTime.
     * Historical prices in Underlying dated after priceTime are ignored, with a warning. 
    
### <code>Opt Cube</code> grounded in FIBO 
An OptCube is a single <code>optPriceTime</code>, plus a <b>finite</b> collection of OptSpecs.
* <code>OptSpec</code> = {
   * optStrike = Strike Price 
   * optExpireTime = Expiration date+time
   * optRight = {OptRtCall | OptRtPut}, 
   * optExcercise = (OptExcEuro | OptExcAmer)
   * Other pertinent facts about excercise rules, trading exchange, 
settlement currency and rules for the option itself (e.g. CBOE), which do not 
overlap with Underlying space above. <br/>} 
 * optPriceTime = is the hypothetical as-of trading time for which this optionCube 
is to be evaluated, using the info available in Underlying (above) and EvalPrefs (below).
 * All OptCube info is interpreted using <b>closed-world</b> semantics.
  * The intrinsicValue portion of an optPrice is always calculated based on the
Underlying Equity price info at optPriceTime.  (That price info might be any
historical trade and bid+ask information, up to within split-seconds of optPriceTime.
If optPriceTime is actually later, a minute or a month after our last equity price 
info, then genPricer() may do its best to guess what the price range is now, 
using the same techniques it uses to guess at "future" prices extending out 
from optPriceTime to optExpireTime)

#### Structure of OptCube instance data - Grounded array or graph?

The OptCube data in a PriceScenario consists of multiple OptSpecs, for a single underlying.
<br/>The OptSpecs have natural ordering in terms of optStrike, optExpireTime, and optRight.
<br/>It is natural to think of OptCube as a multidimensional array, indexed by these components.
<br/>Thus it may be desirable to represent OptCube data in an array data structure, rather than 
as a graph.
<br/>However, we still expect the symbols and constants used in this structure to be grounded
in the FIBO ontology, generally using references to URIs, such as QNames.

### <code>EvalPrefs</code> - generally <b>not</b> grounded in FIBO

EvalPrefs serve several functions:
 * During algorithm development, coders may pass in arbitrary data as scaffolding.
 * As genPricer() grows, EvalPrefs allow switching and tuning of features and assumptions.
 * In a broader algorithm, we can evaluate genPricer() over many combos of such args, and remix those results.

Given these pragmatic switching + scaffolding purposes, it seems unlikely that the 
EvalPrefs will have significant grounding in domain ontology.  
Thus we choose to mostly ignore EvalPrefs for FIBO_study purposes, and focus on the 
Underlying and OptCube sections.  
 
 ## Summarizing our model representation so far

EquityOption-PricingScenario = (Underlying, OptCube, EvalPrefs)
 * Underlying = (graph.UnderlyingEquity, graph.UnderlyingGlobal) = A disjoint (or consistent) pair of graphs, grounded in FIBO.  Mix of open and closed semantics.
 * OptCube = Ordered (two/three level) collection of OptSpecs, indexed by optExpireTime, optStrikePrice, optRight.  Attributes are grounded in FIBO.  Closed-world semantics.
 * EvalPrefs = Not interesting from a domain modeling perspective, as of 2019Q1. 

## Going Deeper - Specific Vocabulary Terms
 * [Collecting our FIBO vocabulary terms](FIBO_dev_Equity_Options_asOf_v2018Q4.md)