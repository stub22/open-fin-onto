# Financial data ontologies - applied to valuation, risk, and compliance

## Background links for orientation
* OMG materials
  * [Finance domain page at OMG](https://www.omg.org/industries/finance.htm)
  * [Intro to FIBO + FIGI at OMG](https://www.omg.org/hot-topics/finance.htm)
* EDM Council materials
  * [Beginner materials](https://edmcouncil.org/page/aboutfiboreview) including "FIBO Primer" PDF.
  * [Overview of FIBO specs](https://spec.edmcouncil.org/) from EDM Council.
  * [FiboPedia](https://spec.edmcouncil.org/fibo/fibopedia/master/2018Q4/FIBOpedia.html) - revised 2018 Q4
* Fin-Reg-Ont materials from Jayzed.com, led by Jurgen Ziemer
  * http://finregont.com = Tutorials and regulatory application scenarios, e.g.: ["Semantic Compliance in Finance"](http://finregont.com/)
  * http://hedgefundontology.com = application of "Hedge Fund Regulation Ontology"
  * Example alignment of financial and legal vocabs:  http://finregont.com/ontology-directory-files-prefixes/
* Beyond ontologies:  Rules, constraints, functions
  * General FIBO rule language issues and future requirements.
    * [FIBO Rule language considerations](https://wiki.edmcouncil.org/display/FIBO/FIBO+Rules+Language+Consideratons) - notes from EDM FIBO group in 2016
  * [Shape Specs and Tools](rdf_shapes/shapeSpecsAndTools.md) - Exploring SHACL and ShEx vocabularies for schemas + constraints
  * What about [Julia](https://lectures.quantecon.org/jl/)?  - (link to QuantEcon.org lectures on using [Julia language](https://julialang.org/) in finance + econ)
    * [Miletus](https://juliacomputing.com/products/juliafin/miletus.html) - Compare and contrast with our FIBO + FP approach to option contracts.
    * [QuantEcon lib for Julia](https://github.com/QuantEcon/QuantEcon.jl) - What tradeoffs does this library bring?
## Our purpose in this "open-fin-onto" project
Explore 3 kinds of application of data alignment using explicit finance vocabularies, e.g. FIBO:
1. Risk - models of event likelihood and impact.  
2. Valuation - assets, liabs, strms, opps, risks.
3. Compliance - reporting complex reality in structured, itemized form.
 
## Metadata imported from EDM council 
* [Numerous FIBO formats and glossaries](https://spec.edmcouncil.org/fibo/) - at EDM Council.
* [FIBO metadata download + setup instructions](https://spec.edmcouncil.org/static/ontology/index.html) from EDM Council. 
* Our o-f-o Gitlab work uses 2018-Q4 production FIBO, in turtle and .nq formats.
  * Link to [prod.ttl.zip](https://spec.edmcouncil.org/fibo/ontology/master/2018Q4/prod.ttl.zip) 

## "open-fin-onto" project - Software Toolset:
* [Dmo_Fibo](https://github.com/stub22/axiomagic/tree/master/adaxmvn/axmgc_dmo_fibo) Query and Viz prototype, uses Scala and Akka Http
  * Build with maven or IntelliJ, then run axmgc.dmo.fin.ontdmp.TstOntDmps, which does two things:
    1. Prints some query results and statistics to the stdout console (and other log destinations).
    2. Launches a web service on port 8119.

### "FIBO Study" sub-project - structure exploration
* [Preliminary results of FIBO ontology study](fibo_study/FIBO_model_study_2018Q4.md) 
* [Using FIBO vocabulary to define Underlying and OptCube](fibo_study/opt_pricer_grnd_fibo.md) inputs to genPricer() function in our "Option Price" example (next section below).

### "Option Price" sub-project (example of valuation application)
* [An option valuation challenge example](option_price/README_option_price.md) -defined on 2019-Feb-22.
  * Pricer[OptCube] : (opt : ElementOf[OptCube]) ->  MoneyValue
  * Generator : (Underlying, OptCube, Prefs) -> Pricer[OptCube]
    * where OptCube defines one time of evaluation (optPriceTime), plus range of strike prices, expirys, put/call rights for a single Underlying.  
    * where Underlying includes selection of models of economic processes relevant to valuing a tradable equity, which is snapshot by Generator into a time-invariant Pricer func.
