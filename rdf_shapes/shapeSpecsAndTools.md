

  * SHACL shapes constraint language, championed by Top Quadrant and U. Leipzig.
    * [FIBO in SHACL: The Next Step in Data Validation and Interoperability](https://www.topquadrant.com/resources/blogs/semantic-ecosystem-journal/docs/FIBO-in-SHACL.pdf) (PDF) - Irene Polikoff's slides from April 2018 
    * [SHACL example data and test case spec](http://datashapes.org/) at Datashapes.org, includes translation of schema.org into SHACL.   
    * [SHACL specs overview](https://www.topquadrant.com/2017/06/12/shacl-features-and-specifications/) 
      * [SHACL formal spec](https://www.w3.org/TR/shacl/) at W3C - defines core features for validation.
      * [SHACL-AF spec](https://www.w3.org/TR/shacl-af) at W3C - defines "Advanced Features" for inference. 
    * [SHACL open source implementation](https://github.com/TopQuadrant/shacl) - in Java, Apache license, thanks to Holger Knoblauch, Andy Seaborne, Antonio Garrotte et al.
    * [SHACL test case data](https://github.com/TopQuadrant/shacl/tree/master/src/test/resources/sh/tests) (from same Shacl impl proj on GitHub)
  * ShEx 
    * [ShEx Primer](http://shex.io/shex-primer/)
  * Comparisons and Combinations
    * https://github.com/labra/shaclex - Scala implementation of both SHACL and ShEx
    * [Comparing ShEx and SHACL](https://book.validatingrdf.com/bookHtml013.html) - Chapter 7 of the book "Validating RDF Data"
    * [Shex vs Shacl](https://www.slideshare.net/jelabra/shex-vs-shacl) - slides